<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;


Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('/aboutUs', [PublicController::class, 'aboutUs'])->name('aboutUs');
Route::get('/servizi', [PublicController::class, 'servizi'])->name('servizi');
Route::get('/informazioni/{id}', [PublicController::class, 'informazioni'])->name('informazioni');