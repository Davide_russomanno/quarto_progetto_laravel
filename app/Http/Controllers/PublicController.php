<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 

class PublicController extends Controller
{

    public $dentisti = [
        [
            'id' => '1',
            'descrizione' => 'Soluzioni efficaci, risultati tangibili e pazienti soddisfatti. Queste sono le tre fasi che caratterizzano il lavoro di realizzazione delle protesi presso lo studio del dott. Piccolo.

            In effetti possiamo proprio dire, senza essere troppo esuberanti, che la protesi è la “punta di diamante” dello studio, in quanto ci avvaliamo di uno dei migliori laboratori odontotecnici con Certificazione di Qualità, utilizzando solo ed esclusivamente materiali di altissima qualità.
            
            La nostra primaria attenzione è sempre puntata sul paziente, per il quale cerchiamo solo il meglio sia in termini di servizi sia in termini di prodotto finale.',
            'servizio' => 'PROTESI',
            'prezzo' => '€5.000',
            'img-2' => '/img/protesi.jpg',
            'img' => '/img/dentista-1.jpg',
            'nome' => 'Vito',
            'titolo' => 'Dentista',
            'anni' => '40'
        ],
        [
            'id' => '2',
            'descrizione' => 'E importante accompagnarli fin da piccoli presso uno studio dentistico e abituarli sia ad un controllo periodico della bocca sia alla corretta igiene orale.

            Solo in questo modo si potrà ottenere un buon riscontro da parte del piccolo paziente che cerca nel dentista una figura capace quasi di proteggerlo dai cattivi batteri che si insediano nella sua bocca.
            
            La nostra mission con loro è quella di prenderli per mano seguendoli e curandoli fino all età evolutiva. Un viaggio alla scoperta di ciò che è positivo e di ciò che è negativo per la salute dentale, quasi come fosse un avventura vera e propria.',
            'servizio' => 'ODONTOIATRIA INFANTILE',
            'prezzo' => '€2.000',
            'img-2' => '/img/infantile.jpg',
            'img' => '/img/dentista-2.jpg',
            'nome' => 'Andrea',
            'titolo' => 'Igienista',
            'anni' => '35'
        ],
        [
            'id' => '3',
            'descrizione' => '“Un sorriso bello e sano” è una frase forse già sentita. Eppure esprime con poche parole un concetto chiaro di bellezza che tutti vogliono e possono raggiungere.

            La prevenzione e la buona igiene dentale sono fondamentali per garantire nel tempo la salute completa di tutta la bocca.
            
            Presso lo studio Piccolo effettuiamo ablazione tartaro e sbiancamento professionale tramite apparecchiature di ultima generazione. Senza contare i consigli e i suggerimenti che possiamo darti per ottenere con facilità il sorriso che hai sempre desiderato.',
            'servizio' => 'IGIENE E PREVENZIONE',
            'prezzo' => '€120.00',
            'img-2' => '/img/igiene.jpg',
            'img' => '/img/dentista-3.jpg',
            'nome' => 'Luca',
            'titolo' => 'Assistente',
            'anni' => '24'
        ],
        [
            'id' => '4',
            'descrizione' => 'La possibilità di comunicare con gli altri parte dalla nostra bocca ,dal nostro sorriso .

            Proprio per assicurarti un sorriso splendente in ogni momento, lo studio si occupa di odontoiatria estetica, con particolare attenzione alla riabilitazione estetico-funzionale. Dalla semplice otturazione estetica alle corone in ceramica e faccette estetiche, grazie ai nostri consigli e interventi, ottenere il sorriso che hai sempre desiderato sarà facile ed immediato!',
            'servizio' => 'ODONTOIATRIA ESTETICA',
            'prezzo' => '€3.000',
            'img-2' => '/img/estetica.jpg',
            'img' => '/img/segretaria.jpg',
            'nome' => 'Pia',
            'titolo' => 'Segretaria',
            'anni' => '32'
        ],
    ];

    public function home() {
        return view('welcome');
    }

    public function aboutUs() {
        return view('aboutUs', ['dentisti'=> $this->dentisti]);
    }

    public function servizi() {
        return view('servizi', ['dentisti'=> $this->dentisti]);
    }

    public function informazioni($id) {
        
        foreach($this->dentisti as $dentista){
            if($dentista['id'] == $id){
                return view('informazioni', ['dentista' => $dentista]);
            }
        }
    }
}
