<x-layout>

    <div class="sfondo d-flex justify-content-center">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 my-5">
                    <img src="{{ $dentista['img-2'] }}" alt="{{ $dentista['servizio'] }}" class="img-fluid">
                </div>
                <div class="col-12 col-md-6 my-5">
                    <h2 class="me-2">{{ $dentista['servizio'] }}</h2>
                    <h4 class="my-3">Descrizione: {{ $dentista['descrizione'] }}</h4>
                    <h4 class="my-4">Prezzo: {{ $dentista['prezzo'] }}</h4>
                </div>
            </div>
        </div>
    </div>

</x-layout>
