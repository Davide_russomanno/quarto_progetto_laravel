<x-layout>
    
    <div class="container-fluid sfondo d-flex justify-content-center align-items-center">
        <div class="row">
            <div class="col-12">
                <h1 class="text-brand">La nostra forza è il tuo sorriso</h1>
                <img src="/img/sorriso.png" alt="">
                <a href="{{ route('servizi') }}">
                    <button class="button-19 my-5" role="button">Dettagli servizi</button>
                </a>
            </div>
        </div>
    </div>

</x-layout>