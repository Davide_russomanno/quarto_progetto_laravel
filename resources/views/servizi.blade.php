<x-layout>

    <div class="sfondo-3 d-flex align-items-center">
        <div class="container-fluid">
            <div class="row">
            @foreach($dentisti as $dentista)
                <div class="col-12 contenitore-servizi">
                    <div class="gallery d-flex">
                        <img src="{{ $dentista['img-2'] }}" alt="{{ $dentista['servizio'] }}" class="mb-5">
                        <h1 class="posizione">{{ $dentista['servizio'] }}</h1>
                        <h5 class="posizione-2">{{ substr($dentista['descrizione'], 0, 300) }}...</h5>
                        <a href="{{ route('informazioni', ['id' => $dentista['id']]) }}">
                            <button class="button-18" role="button">Più informazioni</button>
                        </a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>

</x-layout>