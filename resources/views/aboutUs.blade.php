<x-layout>

    <div class="sfondo-2 d-flex align-items-center">
        <div class="container-fluid">
            <div class="row">
                @foreach($dentisti as $dentista)
                    <div class="col-12 col-md-3">
                        <div class="flip-card">
                            <div class="flip-card-inner">
                                <div class="flip-card-front">
                                    <img src="{{ $dentista['img'] }}" alt="{{ $dentista['nome'] }}">
                                </div>
                                <div class="flip-card-back">
                                    <img src="{{ $dentista['img'] }}" alt="{{ $dentista['nome'] }}">
                                    <h1>{{ $dentista['nome'] }}</h1>
                                    <h3>Titolo: {{ $dentista['titolo'] }}</h3>
                                    <h3>Anni: {{ $dentista['anni'] }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

</x-layout>